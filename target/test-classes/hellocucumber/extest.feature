Feature: An example

  Scenario Outline: Pesquisar vôo e validar o local de partida escolhido.

    Given Abrir o browser
    And que o usuario esteja na pagina Flights
    When preencher o "<localDePartida>" e preencher o "<localDeDestino>" e a data de partida E clicar no botao pesquisar
    Then sera exibido a cartela de voos com os "<dados>" selecionados.

    Examples:
      | localDePartida                                        | localDeDestino                                             |dados|
      | REC - Guararapes Gilberto Freyre Intl - Recife | GRU - Guarulhos Gov Andre Franco Montouro - Sao Paulo |Recife Airport"|

  Scenario Outline: Validar obrigatoriedade dos campos.

    Given Abrir o browser
    And que o usuario esteja na pagina Flights
    When preencher o "<localDePartida>" e preencher o "<localDeDestino>" e a data de partida E clicar no botao pesquisar
    Then sera exibido um alerta de "<erro>" no navegador.

    Examples:
      | localDePartida                                        | localDeDestino                                               |erro|
      || GRU - Guarulhos Gov Andre Franco Montouro - Sao Paulo |Please fill out origin|
      |REC - Guararapes Gilberto Freyre Intl - Recife|  |Please fill out destination|


  Scenario Outline: Validar filtro de pesquisa de vôos sem
  escala.

    Given Abrir o browser
    And que o usuario esteja na pagina Flights
    When preencher o "<localDePartida>" e preencher o "<localDeDestino>" e a data de partida E clicar no botao pesquisar
    And Clicar no radioButton Direct e validar paradas
    Then sera exibido a cartela de resultados de "<paradas>"

    Examples:
      | localDePartida                                        | localDeDestino                                 |paradas|
      | REC - Guararapes Gilberto Freyre Intl - Recife | GRU - Guarulhos Gov Andre Franco Montouro - Sao Paulo |Stops 0|


  Scenario Outline: Pesquisar vôo e validar local de Destino escolhido.

    Given Abrir o browser
    And que o usuario esteja na pagina Flights
    When preencher o "<localDePartida>" e preencher o "<localDeDestino>" e a data de partida E clicar no botao pesquisar
    Then sera exibido a cartela de resultados de voos "<destino>"

    Examples:
      | localDePartida                                        | localDeDestino                                |destino|
      | REC - Guararapes Gilberto Freyre Intl - Recife | GRU - Guarulhos Gov Andre Franco Montouro - Sao Paulo |SAO|












