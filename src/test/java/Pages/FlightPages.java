package Pages;

import org.openqa.selenium.WebElement;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class FlightPages {
    WebDriver driver;
    public FlightPages(WebDriver driver){this.driver = driver;
       // PageFactory.initElements(driver,FlightPages.class);

    }

    @FindBy(id = "autocomplete")
    private WebElement localPartida;

    @FindBy(xpath = "//div[@class=\\\"autocomplete-result\\\"][@data-index=\\\"0\\\"]")
    private WebElement primeiraOpcaoDropDown;

    @FindBy(id = "autocomplete2")
    public WebElement destino;

    @FindBy(id = "departure")
    public WebElement dataPartida;

    @FindBy(xpath ="//i[@class=\"icon mdi mdi-long-arrow-right\"]")
    public WebElement flechaDireitaData;

    @FindBy(xpath ="//td[@class=\"day \"][text()=\"15\"]")
    public WebElement dia15Data;

    @FindBy(id = "flights-search")
    private WebElement botaoPesquisar;


    public void selecionarFlyingFrom(String partida) {
        //localPartida.sendKeys(partida);
        driver.findElement(By.id("autocomplete")).sendKeys(partida);
        Assert.assertEquals(partida, driver.findElement(By.id("autocomplete")).getAttribute("value"));

    }

    public void selecionarToDestination(String localDestino){
        //destino.sendKeys(localDestino);
         driver.findElement(By.id("autocomplete2")).sendKeys(localDestino);
        Assert.assertEquals(localDestino, driver.findElement(By.id("autocomplete2")).getAttribute("value"));


    }

    public void selecionarData3MesesAFrente(){
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.id("departure"))).click().perform();
        actions.moveToElement((driver.findElement(By.xpath("//i[@class=\"icon mdi mdi-long-arrow-right\"]")))).doubleClick().perform();
        actions.moveToElement(driver.findElement(By.xpath("//td[@class=\"day \"][text()=\"15\"]"))).click().perform();
        //Dia20Data.click();
    }




    public void clicarBotãoPesquisarPositivo(){
       // botaoPesquisar.click();
        driver.findElement(By.id("flights-search")).click();

    }



    public void pegarTextoEAceitarAlerta(String textoAlerta){

        Alert alerta = driver.switchTo().alert();
        Assert.assertEquals(textoAlerta, alerta.getText());
        alerta.accept();

    }

    public void selecionarComboClasse(String classeVoo){
       WebElement classe = driver.findElement(By.id("flight_type"));
       Select combo = new Select(classe);
       combo.selectByVisibleText(classeVoo);

    }
}
