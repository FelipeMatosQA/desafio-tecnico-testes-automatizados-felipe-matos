package Pages;
;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    WebDriver driver;
    public HomePage(WebDriver driver){
        this.driver = driver;
       // PageFactory.initElements(driver,HomePage.class);
    }

    @FindBy(className = "active_flights")
    private WebElement rotinaFligths;

    // By rotinaFligths = By.className("active_flights");


    public void acessarRotinaFligths(){
        //rotinaFligths.click();
       driver.findElement(By.className("active_flights")).click();
    }
}
