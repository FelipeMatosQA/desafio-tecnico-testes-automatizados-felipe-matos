package Pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static org.junit.Assert.*;

public class FlightSearchResultPage {
    WebDriver driver;
    public FlightSearchResultPage(WebDriver driver){
      this.driver = driver;
       // PageFactory.initElements(driver,FlightSearchResultPage.class);
    }


    @FindBy(xpath = "//p[@class=\"theme-search-results-item-flight-section-meta-city\"][text()=\"Recife Airport\"]")
    private WebElement aeroportoPartida;

    @FindBy(id = "direct")
    private WebElement radioDirect;

    @FindBy(xpath = "//div[@class=\"theme-search-results-item-flight-section-path-line-middle\"]//strong")
    private WebElement numeroParadas;

    @FindBy(id = "direct")
    private WebElement radioButtonDirect;

    @FindBy(xpath = "//div[@class=\"col-md-6 col-6 g-0\"]//i [@class=\"la la-plane-arrival theme-search-results-item-flight-section-path-icon\"]/following-sibling::div[2]")
    private WebElement aeroportoDestino;


    public  void validarLocalPartida(String partida) {
        //assertEquals(partida, aeroportoPartida.getText());

        assertEquals(partida, driver.findElement(By.xpath("//p[@class=\"theme-search-results-item-flight-section-meta-city\"][text()=\"Recife Airport\"]")).getText());
    }



    public String pegarNumeroParadas() {
       return driver.findElement(By.xpath("//div[@class=\"theme-search-results-item-flight-section-path-line-middle\"]//strong")).getText(); //numeroParadas.getText();
    }


    public void clicarRadioButtonDirect() {
        driver.findElement(By.id("direct")).click();
        //radioDirect.click();
    }

    public String pegarValorAeroportoDestino(){

        return driver.findElement(By.xpath("//div[@class=\"col-md-6 col-6 g-0\"]//i [@class=\"la la-plane-arrival theme-search-results-item-flight-section-path-icon\"]/following-sibling::div[2]")).getText();//aeroportoDestino.getText();
    }

    public void retornarListaDeResultadosAirlines(){
        List<WebElement> options = driver.findElements(By.xpath(".theme-search-results-item-flight-section-airline-title"));

        System.out.println(options.get(0).toString());
    }

    public String retornarValorClasse(){
        return driver.findElement(By.xpath("//p[@class=\"d-flex align-items-center\"]")).getText();

    }

}
