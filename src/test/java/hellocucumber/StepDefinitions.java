package hellocucumber;

import Pages.FlightPages;
import Pages.FlightSearchResultPage;
import Pages.HomePage;
import io.cucumber.java.AfterAll;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.lang.System;
import java.util.concurrent.TimeUnit;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class StepDefinitions {
    WebDriver driver = null;

    @Rule
    public TestName testName = new TestName();

    @Given("Abrir o browser")
    public void abrirBrowser(){

        String projectPath = System.getProperty("user.dir");
        System.setProperty("websdriver.chromedriver",projectPath +"src/test/resources/drivers/chromedriver.exe");
        WebDriverManager.chromedriver().setup();
        driver= new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.navigate().to("https://phptravels.net/");

    }

    @io.cucumber.java.After
    public void fecharBrowser() throws IOException {

        TakesScreenshot print =(TakesScreenshot)driver;
        File arquivo = print.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(arquivo, new File("target/" + testName.getMethodName() + ".jpg"));

        driver.quit();

    }



    @And("que o usuario esteja na pagina Flights")
    public void que_o_usuario_esteja_na_pagina_flights() {

       HomePage home = new HomePage(driver);
       home.acessarRotinaFligths();

    }
    @When("preencher o {string} e preencher o {string} e a data de partida E clicar no botao pesquisar")
    public void preencher_o_e_preencher_o_e_a_data_de_partida_e_clicar_no_botao_pesquisar(String string, String string2) {
        FlightPages flightPages = new FlightPages(driver);
        flightPages.selecionarFlyingFrom(string);
        flightPages.selecionarToDestination(string2);
        flightPages.selecionarData3MesesAFrente();
        flightPages.clicarBotãoPesquisarPositivo();

    }
    @When("selecionar combo {string}")
    public void selecionar_combo_classe(String string){
        FlightPages flightPages = new FlightPages(driver);
        flightPages.selecionarComboClasse(string);

    }
    @Then("sera exibido a cartela de voos com os {string}\" selecionados.")
    public void sera_exibido_a_cartela_de_voos_com_os_selecionados(String string) {
        FlightSearchResultPage flightSearchResultPage = new FlightSearchResultPage(driver);
        flightSearchResultPage.validarLocalPartida(string);

    }

    @Then("sera exibido um alerta de {string} no navegador.")
    public void sera_exibido_um_alerta_de_no_navegador(String string) {
        FlightPages flightPages = new FlightPages(driver);
        flightPages.pegarTextoEAceitarAlerta(string);
    }

    @When("Clicar no radioButton Direct e validar paradas")
    public void clicar_no_radio_button_direct_e_validar_paradas() {
        FlightSearchResultPage flightSearchResultPage = new FlightSearchResultPage(driver);
        flightSearchResultPage.clicarRadioButtonDirect();
    }
    @Then("sera exibido a cartela de resultados de {string}")
    public void sera_exibido_a_cartela_de_resultados_de(String string) {
        FlightSearchResultPage flightSearchResultPage = new FlightSearchResultPage(driver);
       Assert.assertEquals(string, flightSearchResultPage.pegarNumeroParadas());
    }

    @Then("sera exibido a cartela de resultados de voos {string}")
    public void sera_exibido_A_cartela_de_resultados_de_voos_destino(String string){
        FlightSearchResultPage flightSearchResultPage = new FlightSearchResultPage(driver);
        Assert.assertEquals(string,flightSearchResultPage.pegarValorAeroportoDestino());
    }

}
