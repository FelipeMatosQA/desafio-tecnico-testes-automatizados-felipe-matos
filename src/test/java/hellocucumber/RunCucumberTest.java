package hellocucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;
import org.junit.runner.RunWith;
;

//@Suite
//@IncludeEngines("cucumber")
//@SelectClasspathResource("hellocucumber")
//@ConfigurationParameter(key = PLUGIN_PROPERTY_NAME, value = "pretty")

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/hellocucumber/extext.feature",glue = {"StepDefinitions"})
public class RunCucumberTest {

}
